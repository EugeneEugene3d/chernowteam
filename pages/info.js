export default function Info() {
  return (
    <main>
      <h2>Video editing specs</h2>
      <div>Phone Screencast: 576 × 1280</div>

      <h2>Tool to normalize framerate:</h2>
      <div>ffmpeg -i input.mp4 -filter:v fps=25 output.mp4</div>
    </main>
  )
}
